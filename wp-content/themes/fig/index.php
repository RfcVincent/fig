<?php get_header();

if (have_posts()): ?>

    <div class="page-contents">

    <?php

        while (have_posts()): the_post();

            if (have_rows('layout')):

                while (have_rows('layout')): the_row();

                    get_template_part('partials/' . get_row_layout());

                endwhile;

            endif;

        endwhile;

    ?>

      <button class="back-to-top js-scroll-top">
        <span class="back-to-top__btn"></span>
        <span>
            Back To Top
          </span>
      </button>

    </div>

<?php endif;

get_footer();
