<?php
$url         = get_sub_field( 'video', false, false );
$oembed      = _wp_oembed_get_object();
$provider    = $oembed->get_provider( $url );
$oembed_data = $oembed->fetch( $provider, $url );
$title       = $oembed_data->title;

$iframe = get_sub_field( 'video' );
preg_match( '/src="(.+?)"/', $iframe, $matches );
$src = $matches[1];

if ( ! $src ) {
  return;
}

preg_match( "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", get_sub_field( 'video', false, false ), $id );
$thumbnail = 'http://i.ytimg.com/vi/' . $id[0] . '/maxresdefault.jpg';

$params     = array(
  'controls'       => 0,
  'hd'             => 1,
  'modestbranding' => 1,
  'enablejsapi'    => 1,
  'loop'           => 1,
  'autoplay'       => 1
);
$new_src    = add_query_arg( $params, $src );
$iframe     = str_replace( $src, $new_src, $iframe );
$attributes = 'frameborder="0"';
$iframe     = str_replace( '></iframe>', ' ' . $attributes . '></iframe>', $iframe );

?>

<div class="video-container js-video">
  <img
      src="<?php echo $thumbnail; ?>"
      alt="<?php echo $title; ?>"
      class="video-container__thumbnail"
  >
  <div class="video-container__play-btn"></div>
  <?php echo $iframe; ?>
</div>
