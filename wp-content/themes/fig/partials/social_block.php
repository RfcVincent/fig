<section class="social-block">

  <h4 class="title title--center title--block social-block__title">
    Follow Us
  </h4>

  <?php

  function wp_instagram_feed($access_token) {

    $cURLConnection = curl_init();
    curl_setopt($cURLConnection, CURLOPT_URL, "https://graph.instagram.com/me/media?fields=id,media_url,permalink&limit=18&access_token=" . $access_token);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

    $media = curl_exec($cURLConnection);
    curl_close($cURLConnection);

    $jsonArrayResponse = json_decode($media);

    return $jsonArrayResponse;

  }

  $data = wp_instagram_feed($access_token = get_transient( 'insta_access_token' ));

  $images = [];
  foreach ( $data->data as $index => $post ) {
    $images[ $index ]['media_url'] = $post->media_url;
    $images[ $index ]['permalink']  = $post->permalink;
    $images[ $index ]['id']    = $post->id;
  }
  ?>

  <div class="social-block__images">
    <div class="social-block__images-wrapper js-instafeed"></div>
  </div>

  <a
      href="https://instagram.com/<?php echo get_field( 'instagram', 'option' ); ?>"
      class="social-block__link"
      target="_blank"
      rel="nofollow noopener"
  >
    <?php echo file_get_contents( get_template_directory() . "/src/img/instagram.svg" ); ?> Instagram
  </a>

  <button class="js-insta-more btn">
    Load More
  </button>

</section>

<script>
  const feed = document.querySelector('.js-instafeed');
  const more = document.querySelector('.js-insta-more');
  let paging = <?php echo json_encode( $data->paging->next, JSON_HEX_TAG ); ?>;
  let data = <?php echo json_encode( $images, JSON_HEX_TAG ); ?>;

  let isAsync = true;

  try {
    eval('async () => {}');
  } catch (e) {
    if (e instanceof SyntaxError)
      isAsync = false;
    else
      throw e; // throws CSP error
  }

  if(!isAsync) {
    more.style.display = 'none';
  }

  async function returnNext() {
    return await (await fetch(paging)).json();
  }

  more.addEventListener('click', async () => {
    const updatedData = await returnNext();
    paging = updatedData.paging.next;
    data = [...updatedData.data];

    updateItems();
  })

  updateItems();

  function updateItems() {
    if(data.length) {

      data.forEach(item => {
        feed.innerHTML += `
      <a
              class="lazy-container lazyload social-block__image"
              data-expand="-100"
              href="${item.permalink}"
          >
            <img
                data-src="${item.media_url}"
                class="lazyload"
            >

          </a>
      `
      });

      feed.dataset.last = data[data.length - 1].id;

      }
  }
</script>
