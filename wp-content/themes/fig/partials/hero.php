<section class="hero">

  <div class="image-slider js-slider-container">

    <div class="js-slider image-slider__slider">

      <?php foreach ( get_sub_field( 'slides' ) as $image ) { ?>

        <div
            class="image-slider__slide image-slider__slide--alt"
            data-flickity-bg-lazyload="<?php echo $image['image']['sizes']['image-slider']; ?>"
        >

          <div class="container container--md">

            <div class="image-slider__contents<?php echo $image['content_width'] === 'wide' ? ' image-slider__contents--wide' : null ?>">

              <?php if ( $image['title'] ) { ?>
                <h1
                    class="title lazyload lazy-content lazy-content--delay lazy-content--right"
                    data-expand="100"
                >
                  <?php echo $image['title']; ?>
                </h1>
              <?php } ?>

              <?php if ( $image['subtitle'] ) { ?>
                <span
                    class="image-slider__subtitle lazyload lazy-content lazy-content--delay lazy-content--right"
                    data-expand="100"
                >
                  <?php echo $image['subtitle']; ?>
                </span>
              <?php } ?>

              <?php if ( $image['subtitle_2'] ) { ?>
                <span
                    class="image-slider__subtitle image-slider__subtitle--bottom lazyload lazy-content lazy-content--delay lazy-content--right"
                    data-expand="100"
                >
              <?php echo $image['subtitle_2']; ?>
            </span>
              <?php } ?>

              <?php if ( $image['link'] ) { ?>
                <a
                    href="<?php echo $image['link']['url'] ?>"
                    target="<?php echo $image['link']['target'] ?>"
                    class="image-slider__link arrow-link lazyload lazy-content lazy-content--delay lazy-content--right"
                    data-expand="100"
                >
                  <?php echo $image['link']['title']; ?>
                  <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
                </a>
              <?php } ?>

              <?php if ( $image['search_projects_button'] ) { ?>
                <button class="btn js-open-search-window lazyload lazy-content lazy-content--delay lazy-content--left">
                  Search Projects
                </button>
              <?php } ?>

            </div>

          </div>

          <div class="image-slider__overlay"></div>
        </div>

      <?php } ?>

    </div>

    <?php if ( count( get_sub_field( 'slides' ) ) > 1 ) { ?>

      <div class="image-slider__controls image-slider__controls--absolute js-slider-controls">

        <div class="js-slider-controls image-slider__buttons">
          <button class="image-slider__button image-slider__button--next js-slider-next">
            <?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?>
          </button>
          <button class="image-slider__button image-slider__button--prev js-slider-prev">
            <?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?>
          </button>
        </div>

      </div>

    <?php } ?>

  </div>

  <?php if ( get_field( 'client' ) || get_field( 'services' ) || get_field( 'project_type' ) || get_field( 'region' ) || get_field( 'brand' ) ) { ?>

    <div class="project-details">

      <?php if ( get_field( 'client' ) ) { ?>
        <div class="project-details__detail">
              <span class="project-details__title">
                Client
              </span>
          <?php echo get_field( 'client' ); ?>
        </div>
      <?php } ?>

      <?php if ( get_field( 'services' ) ) { ?>
        <div class="project-details__detail">
              <span class="project-details__title">
                Services
              </span>
          <?php echo get_field( 'services' ); ?>
        </div>
      <?php } ?>

      <?php if ( get_field( 'project_type' ) ) { ?>
        <div class="project-details__detail">
              <span class="project-details__title">
                Project Type
              </span>
          <?php echo get_field( 'project_type' ); ?>
        </div>
      <?php } ?>

      <?php if ( get_field( 'region' ) ) { ?>
        <div class="project-details__detail">
              <span class="project-details__title">
                Region
              </span>
          <?php echo get_field( 'region' ); ?>
        </div>
      <?php } ?>

      <?php if ( get_field( 'brand' ) ) { ?>
        <div class="project-details__detail">
              <span class="project-details__title">
                Brand
              </span>
          <?php echo get_field( 'brand' ); ?>
        </div>
      <?php } ?>

    </div>

  <?php } ?>

  <?php if ( get_post_type() === 'projects' ) { ?>
    <div class="container container--xlrg project-back">
      <button
          class="btn"
          onclick="window.history.back();"
      >
        Back to search
      </button>
    </div>
  <?php } ?>

</section>

<?php
$windowActive = false;
foreach ( get_sub_field( 'slides' ) as $image ) {
  if ( $image['search_projects_button'] && ! $windowActive ) {
    include_once "search-window.php";
    $windowActive = true;
  }
}

?>
