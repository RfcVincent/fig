<section class="news-grid-container">

  <div class="news-grid">

    <?php
    $posts = get_posts( [
      'post_type'   => 'news',
      'post_status' => 'publish',
      'numberposts' => - 1
    ] );
    ?>

    <?php foreach ( $posts as $index => $post ) { ?>

      <a
          href="<?php echo get_permalink( $post->ID) ?>"
          class="news-grid-image lazy-container lazyload"
          data-expand="-100"
      >

        <div style="position: absolute; width: 100%; height: 100%;">
          <img
              data-src="<?php echo wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), $size = 'news-image-lrg' ) ?>"
              alt="<?php echo $post->post_title; ?>"
              class="news-grid-image__bg lazyload lazy-img lazy-img--outa"
          >
          <span class="lazy-borders">
            <span class="lazy-borders__border lazy-borders__border--top"></span>
            <span class="lazy-borders__border lazy-borders__border--right"></span>
            <span class="lazy-borders__border lazy-borders__border--bottom"></span>
            <span class="lazy-borders__border lazy-borders__border--left"></span>
          </span>
          <div class="news-grid-image__overlay">
            <div class="news-grid-image__content">
              <span class="title">
                <?php echo strlen( $post->post_title ) > 40 ? substr( $post->post_title, 0, 40 ) . "..." : $post->post_title; ?>
              </span>
                  <span class="date" style="margin-top: 10px;">
                  <?php echo get_the_date('l F j, Y', $post->ID) ?>
                </span>
                  <span class="arrow-link">
                Read Article <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
              </span>
            </div>
          </div>
        </div>

      </a>

      <a href="<?php echo get_permalink( $post->ID) ?>" class="news-grid-image__content news-grid-image__content--mobile">
          <span class="title">
            <?php echo strlen( $post->post_title ) > 40 ? substr( $post->post_title, 0, 40 ) . "..." : $post->post_title; ?>
          </span>
        <span class="date" style="margin-top: 10px;">
              <?php echo get_the_date('l F j, Y', $post->ID) ?>
            </span>
        <span class="arrow-link">
            Read Article <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
          </span>
      </a>

    <?php } ?>

  </div>

  <!--  <button class="arrow-link news-grid-container__view-more">-->
  <!--    View More-->
  <!--    <span class="arrow-link__arrow">-->
  <?php //echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?><!--</span>-->
  <!--  </button>-->

</section>
