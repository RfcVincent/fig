<section class="content-block-container">

  <div class="container container--md">

    <div class="content-block content-block-layout--<?php echo get_sub_field( 'column_layout' ) ?>">

      <?php if ( get_sub_field( 'column_count' ) === 'one' ) { ?>

        <div
            class="content-block__col content-block__col--single content-block__col--wide lazyload lazy-content-container"
            data-expand="-300"
        >

          <?php if ( get_sub_field( 'column' )['sub_title'] ) { ?>
            <div class="content-block__content">
              <p class="content-block__subtitle<?php echo get_sub_field( 'column' )['sub_title_size'] ? ' content-block__subtitle--sm' : null ?>">
                <?php echo get_sub_field( 'column' )['sub_title']; ?>
              </p>
            </div>
          <?php } ?>

          <?php if ( get_sub_field( 'column' )['title'] ) { ?>
            <h2 class="title">
              <?php echo get_sub_field( 'column' )['title']; ?>
            </h2>
          <?php } ?>

          <?php if ( get_sub_field( 'column' )['content'] ) { ?>
            <div class="content-block__content">
              <?php echo get_sub_field( 'column' )['content']; ?>
            </div>
          <?php } ?>

          <?php if ( get_sub_field( 'column' )['image'] ) { ?>

            <img
                data-src="<?php echo get_sub_field( 'column' )['image']['sizes']['image-block-md']; ?>"
                alt="<?php echo get_sub_field( 'column' )['image']['alt']; ?>"
                class="content-block__image lazyload"
            >

          <?php } ?>

        </div>

      <?php } else { ?>

      <div
          class="content-block__col<?php echo get_sub_field( 'column_width' ) === 'column1' ? ' content-block__col--wide' : ''; ?><?php echo get_sub_field( 'columns' )['column_1']['image'] || get_sub_field( 'columns' )['column_1']['gallery'] ? ' content-block__col--margin-sm' : null; ?><?php echo get_sub_field( 'columns' )['column_2']['image'] || get_sub_field( 'columns' )['column_2']['gallery'] ? ' content-block__col--margin-sm' : null; ?> lazyload lazy-content-container"
          data-expand="-300"
      >

        <?php if ( get_sub_field( 'columns' )['column_1']['sub_title'] ) { ?>
          <div class="content-block__content">
            <p class="content-block__subtitle<?php echo get_sub_field( 'columns' )['column_1']['sub_title_size'] ? ' content-block__subtitle--sm' : null ?>">
              <?php echo get_sub_field( 'columns' )['column_1']['sub_title']; ?>
            </p>
          </div>
        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_1']['title'] ) { ?>
          <h2 class="title">
            <?php echo get_sub_field( 'columns' )['column_1']['title']; ?>
          </h2>
        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_1']['content'] ) { ?>
          <div class="content-block__content">
            <?php echo get_sub_field( 'columns' )['column_1']['content']; ?>
          </div>
        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_1']['image'] ) { ?>

          <img
              data-src="<?php echo get_sub_field( 'columns' )['column_1']['image']['sizes']['image-block-md']; ?>"
              alt="<?php echo get_sub_field( 'columns' )['column_1']['image']['alt']; ?>"
              class="content-block__image lazyload"
          >

        <?php } ?>

      </div>

      <div
          class="content-block__col<?php echo get_sub_field( 'column_width' ) === 'column2' ? ' content-block__col--wide' : ''; ?> <?php echo get_sub_field( 'columns' )['column_2']['image'] ? ' content-block__col--image' : null; ?> lazyload lazy-content-container"
          data-expand="-300"
      >

        <?php if ( get_sub_field( 'columns' )['column_2']['sub_title'] ) { ?>
          <div class="content-block__content">
            <p class="content-block__subtitle<?php echo get_sub_field( 'columns' )['column_2']['sub_title_size'] ? ' content-block__subtitle--sm' : null ?>">
              <?php echo get_sub_field( 'columns' )['column_2']['sub_title']; ?>
            </p>
          </div>
        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_2']['title'] ) { ?>
          <h2 class="title">
            <?php echo get_sub_field( 'columns' )['column_2']['title']; ?>
          </h2>
        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_2']['content'] ) { ?>
          <div class="content-block__content">
            <?php echo get_sub_field( 'columns' )['column_2']['content']; ?>
          </div>
        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_2']['image'] ) { ?>

          <img
              data-src="<?php echo get_sub_field( 'columns' )['column_2']['image']['sizes']['image-block-md']; ?>"
              alt="<?php echo get_sub_field( 'columns' )['column_2']['image']['alt']; ?>"
              class="content-block__image lazyload"
          >

        <?php } ?>

        <?php if ( get_sub_field( 'columns' )['column_2']['gallery'] ) { ?>

        <div class="image-slider image-slider--sm image-slider--outer-dots js-slider-container">

          <div class="js-slider image-slider__slider">

            <?php foreach ( get_sub_field( 'columns' )['column_2']['gallery'] as $image ) { ?>

              <div
                  class="image-slider__slide image-slider__slide--sm lazyload"
                  data-expand="-100"
                  data-bg="<?php echo $image['sizes']['image-block-lrg']; ?>"
              ></div>

            <?php } ?>

          </div>

          <?php } ?>

        </div>

        <?php } ?>

      </div>

    </div>

</section>
