<?php
$query  = $_SERVER["QUERY_STRING"];
$commas = str_replace( '&', ',', $query );
$array  = explode( "&", $query );
?>

<?php if ( $query ) { ?>

  <div class="container container--md filter-box">

    <div class="filter-box__filters-container">
      <span class="filter-box__title">
        Filtered By
      </span>
      <div class="filter-box__filters">
        <?php foreach ( array_unique( $array ) as $filter ) { ?>
          <span class="filter-box__filter">
            <?php echo get_the_category_by_ID( $filter ) ?>
          </span>
        <?php } ?>
      </div>
    </div>

    <button class="filter-box__btn js-clear-all-filters">
      Reset &nbsp; &#10005;
    </button>

  </div>

<?php } ?>

<section>

  <div class="all-projects">

    <?php
    $posts = get_posts( [
      'post_type'   => 'projects',
      'post_status' => 'publish',
      'numberposts' => - 1,
      'category'    => $commas
    ] );
    ?>

    <?php if(count($posts) === 0) { ?>

      <div class="container container--md">
        <p class="all-projects__none">
          Coming soon
        </p>
      </div>

    <?php } ?>

    <?php foreach ( $posts as $post ) { ?>

      <?php $postTags = get_the_category( $post->ID ); ?>

      <div class="all-projects__project">

        <a
            href="<?php echo get_post_permalink( $post->ID ); ?>"
            class="all-projects__project-wrapper lazy-borders-hover"
        >

            <span
                class="lazy-container lazyload all-projects__project-container"
                data-expand="-100"
            >

              <img
                  data-src="<?php echo wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), $size = 'work-image' ) ?>"
                  alt="<?php echo $post->post_title; ?>"
                  class="all-projects__img lazyload lazy-img lazy-img--outa"
              >
              <span class="lazy-borders">
                <span class="lazy-borders__border lazy-borders__border--top"></span>
                <span class="lazy-borders__border lazy-borders__border--right"></span>
                <span class="lazy-borders__border lazy-borders__border--bottom"></span>
                <span class="lazy-borders__border lazy-borders__border--left"></span>
              </span>

            </span>

          <h2 class="title title--xsmm title--alt">
            <?php echo $post->post_title; ?>
          </h2>

        </a>

        <div class="all-projects__tags lazyload" data-expand="-100">
          <?php foreach ( $postTags as $tag ) { ?>
            <?php if ( $tag->parent !== 0 ) { ?>
              <a
                  href="/work/?<?php echo $tag->term_id ?>"
                  class="all-projects__tag"
              >
                <?php echo $tag->name ?>
              </a>
            <?php } ?>
          <?php } ?>
        </div>

      </div>

    <?php } ?>

    <?php wp_reset_query(); ?>

  </div>

</section>
