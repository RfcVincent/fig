<section>

  <div class="container container--lrg">

    <div class="showcase-image">

      <img
          src="<?php echo get_sub_field( 'showcase_image' )['sizes']['showcase-image']; ?>"
          alt="<?php echo get_sub_field( 'showcase_image' )['alt']; ?>"
          class="showcase-image__image"
      >

    </div>

  </div>

</section>
