<?php if ( count( get_sub_field( 'image_slider' ) ) ) { ?>

  <div
      class="image-slider lazy-container lazyload js-slider-container"
      data-expand="-100"
  >
    <span class="lazy-bg lazy-bg--outh"></span>

    <div class="js-slider image-slider__slider">

      <?php foreach ( get_sub_field( 'image_slider' ) as $key => $image ) { ?>

        <div
            class="image-slider__slide lazyload"
            data-expand="-100"
            data-bg="<?php echo $image['image']['sizes']['image-slider']; ?>"
        >
          <span class="title js-slider-title image-slider__title">
            <?php echo $image['title']; ?>
          </span>

          <?php if ( $image['link'] ) { ?>

            <a
                href="<?php echo $image['link']['url'] ?>"
                target="<?php echo $image['link']['target'] ?>"
                class="arrow-link image-slider__title"
            >
              <?php echo $image['link']['title'] ?>
              <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
            </a>

          <?php } ?>

          <div class="image-slider__overlay"></div>
        </div>

      <?php } ?>

    </div>

    <div class="js-slider-controls image-slider__buttons">
      <button class="image-slider__button image-slider__button--next js-slider-next">
        <?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?>
      </button>
      <button class="image-slider__button image-slider__button--prev js-slider-prev">
        <?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?>
      </button>
    </div>

  </div>

<?php } ?>
