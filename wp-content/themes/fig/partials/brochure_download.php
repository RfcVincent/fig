<section class="brochure-download">

  <div class="container container--md">

    <?php if ( get_sub_field( 'title' ) ) { ?>
      <h2 class="title title--center">
        <?php echo get_sub_field( 'title' ) ?>
      </h2>
    <?php } ?>

    <div class="brochure-download-form">

      <?php if ( get_sub_field( 'copy' ) ) { ?>
        <p class="brochure-download-form__copy">
          <?php echo get_sub_field( 'copy' ) ?>
        </p>
      <?php } ?>

      <?php echo do_shortcode('[contact-form-7 id="116" title="Mailing List"]') ?>

    </div>

  </div>

</section>
