<?php
$parents = get_terms( array(
  'taxonomy'   => 'category',
  'hide_empty' => false,
  'parent'     => 0,
  'exclude'    => 1, // Hide default Uncategorised term
) );
?>

<div class="search-window js-search-window">

  <button class="search-window__close js-close-search-window">
    Close &nbsp; &#10005;
  </button>

  <div class="terms">

    <h2 class="terms__title terms__title--all js-clear-all-filters">
      View all
    </h2>

    <?php
    $order = ['sectors', 'types', 'regions', 'disciplines'];

    usort($parents, function($a, $b) use ($order) {
      $pos_a = array_search($a->slug, $order);
      $pos_b = array_search($b->slug, $order);
      return $pos_a - $pos_b;
    });
    ?>

    <?php foreach ( $parents as $parent ) { ?>

      <div class="terms__column">

        <h2 class="terms__title">
          <?php echo $parent->name ?>
        </h2>

        <?php
        $children = get_terms( array(
          'taxonomy'   => 'category',
          'hide_empty' => false,
          'childless'  => true,
          'parent'     => $parent->term_id,
          'exclude'    => 1, // Hide default Uncategorised term
        ) );
        ?>

        <div class="terms__items">

          <?php foreach ( $children as $child ) { ?>

            <div
                class="terms__item js-search-item"
                data-value="<?php echo $child->term_id ?>"
            >
              <?php echo $child->name ?>
            </div>

          <?php } ?>

        </div>

      </div>

    <?php } ?>

  </div>

</div>
