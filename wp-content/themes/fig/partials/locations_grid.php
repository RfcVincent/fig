<?php $locations = get_field( 'location', 'option' ); ?>

<section class="locations-grid">

  <div class="container container--md">

    <div class="locations-grid__featured">

      <div class="locations-grid__item">

        <div
            class="locations-grid__image lazy-container lazyload"
            data-expand="-100"
            data-bg="<?php echo $locations[0]['image']['sizes']['location-image']; ?>"
        >
          <span class="lazy-bg lazy-bg--outh"></span>
        </div>

        <div
            class="locations-grid__content lazyload lazy-content-container"
            data-expand="-100"
        >

          <div class="locations-grid__content__inner">

            <h3 class="locations-grid__country">
              <?php echo $locations[0]['country'] ?>
            </h3>

            <h2 class="title">
              <?php echo $locations[0]['title'] ?>
            </h2>

            <address>
              <?php echo $locations[0]['address'] ?>
            </address>

            <?php if ( $locations[0]['phone_number'] ) { ?>

              <a
                  href="tel:<?php echo $locations[0]['phone_number']; ?>"
                  class="locations-grid__phone"
              >
                T
                | <?php echo $locations[0]['contact_first_name'] ?: null; ?> <?php echo $locations[0]['contact_last_name'] ?: null; ?> <?php echo $locations[0]['phone_number'] ?>
              </a>

            <?php } ?>

            <?php if ( $locations[0]['email_address'] ) { ?>

              <a
                  href="mailto:<?php echo $locations[0]['email_address']; ?>"
                  class="arrow-link locations-grid__email"
              >
                <?php echo $locations[0]['email_address']; ?>
                <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
              </a>

            <?php } ?>

          </div>

        </div>

      </div>

    </div>

    <div class="locations-grid__items">

      <?php foreach ( array_slice( $locations, 1 ) as $key => $location ) { ?>

        <div class="locations-grid__item">

          <div
              class="locations-grid__image lazy-container lazyload"
              data-expand="-100"
              data-bg="<?php echo $location['image']['sizes']['location-image']; ?>"
          >
            <span class="lazy-bg lazy-bg--<?php echo $key % 2 === 0 ? 'left' : 'right' ?>"></span>
          </div>

          <div
              class="locations-grid__content lazyload lazy-content-container lazy-content-container--vertical"
              data-expand="-100"
          >

            <div class="locations-grid__content__inner">

              <h3 class="locations-grid__country">
                <?php echo $location['country'] ?>
              </h3>

              <h2 class="title">
                <?php echo $location['title'] ?>
              </h2>

              <address>
                <?php echo $location['address'] ?>
              </address>

              <?php if ( $location['phone_number'] ) { ?>

                <a
                    href="tel:<?php echo $location['phone_number']; ?>"
                    class="locations-grid__phone"
                >
                  T
                  | <?php echo $location['contact_first_name'] ?: null; ?> <?php echo $location['contact_last_name'] ?: null; ?> <?php echo $location['phone_number'] ?>
                </a>

              <?php } ?>

              <?php if ( $location['email_address'] ) { ?>

                <a
                    href="mailto:<?php echo $location['email_address']; ?>"
                    class="arrow-link locations-grid__email"
                >
                  <?php echo $location['email_address']; ?>
                  <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
                </a>

              <?php } ?>

            </div>

          </div>

        </div>

      <?php } ?>

    </div>

  </div>

</section>
