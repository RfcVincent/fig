<section>

  <div class="contact-block container container--lrg">

    <h2 class="contact-block__title title">
      <?php echo get_sub_field( 'title' ); ?>
    </h2>

    <div class="contact-block__copy">
      <?php echo get_sub_field( 'copy' ); ?>
    </div>

    <form action="#">

      <div class="contact-block__form-wrapper">

        <input
            class="contact-block__input"
            placeholder="Your email address"
            type="email"
            required
        >
        <button
            type="submit"
            class="contact-block__submit arrow-link"
        >
          Sign up <span class="arrow-link__arrow"><?php echo file_get_contents( get_template_directory() . "/src/img/chevron-right.svg" ); ?></span>
        </button>

      </div>

      <div class="contact-block__consent">

        <div class="checkbox-wrap">
          <input
              type="checkbox"
              class="contact-block__checkbox"
              id="consent-checkbox"
              required
          >
          <span class="checkbox-wrap__check"></span>
        </div>

        <label for="consent-checkbox">
          By ticking this box I agree to Fusion Interiors Privacy Policy, and would like to receive emails with special news and updates from Fusion Interiors.
        </label>

      </div>

    </form>

    <?php if ( count( get_sub_field( 'location_links' ) ) ) { ?>

      <div class="contact-block__links">

        <?php foreach ( get_sub_field( 'location_links' ) as $link ) { ?>

          <a
              href="<?php echo $link['link']['url']; ?>"
              class="contact-block__link"
              target="<?php echo $link['link']['target']; ?>"
          >
            <?php echo $link['link']['title']; ?>
          </a>

        <?php } ?>

      </div>

    <?php } ?>

  </div>

</section>
