<section class="container container--md">

  <div class="quote-block">

    <div class="quote-block__quote">

      <span class="quote-mark quote-mark--left">
        <?php echo file_get_contents( get_template_directory() . "/src/img/quote-left.svg" ); ?>
      </span>

      <?php echo get_sub_field( 'quote' ) ?>

      <span class="quote-mark quote-mark--right">
        <?php echo file_get_contents( get_template_directory() . "/src/img/quote-right.svg" ); ?>
      </span>

    </div>

    <div class="quote-block__author">

      <?php echo get_sub_field( 'quote_author' ) ?>

    </div>

  </div>

</section>
