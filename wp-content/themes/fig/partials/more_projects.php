<section>

  <?php
  $posts = get_posts( [
    'post_type'   => 'projects',
    'post_status' => 'publish',
    'numberposts' => 2,
    'orderby'     => 'rand'
  ] );
  ?>

  <div class="more-projects">

    <?php if ( get_sub_field( 'title' ) ) { ?>
      <h2 class="more-projects__title title title--center">
        <?php echo get_sub_field( 'title' ) ?>
      </h2>
    <?php } ?>

    <div class="more-projects__projects">

      <?php foreach ( $posts as $post ) { ?>

        <?php $projectID = $post->ID ?>

        <?php $postTags = get_the_category( $projectID ); ?>

        <div class="more-projects__project-container">

          <a
              href="<?php echo get_post_permalink( $projectID ); ?>"
              class="more-projects__project lazy-borders-hover"
          >

            <span
                class="more-projects__image-container lazy-container lazyload"
                data-expand="-100"
            >

              <picture>
                <source
                    data-srcset="<?php echo get_the_post_thumbnail_url( $projectID, 'featured-image-mobile' ); ?>"
                    media="(max-width: 640px)"
                >
                <img
                    data-src="<?php echo get_the_post_thumbnail_url( $projectID, 'featured-image' ); ?>"
                    class="more-projects__image lazyload lazy-img lazy-img--outa"
                    alt="<?php echo get_the_title( $projectID ) ?>"
                />
              </picture>
              <span class="lazy-borders">
                <span class="lazy-borders__border lazy-borders__border--top"></span>
                <span class="lazy-borders__border lazy-borders__border--right"></span>
                <span class="lazy-borders__border lazy-borders__border--bottom"></span>
                <span class="lazy-borders__border lazy-borders__border--left"></span>
              </span>

            </span>

            <p class="more-projects__title title title--xsmm title--alt">
              <?php echo get_the_title( $projectID ) ?>
            </p>

          </a>

          <div class="all-projects__tags lazyload" data-expand="-100">
            <?php foreach ( $postTags as $tag ) { ?>
              <?php if ( $tag->parent !== 0 ) { ?>
                <a
                    href="/work/?<?php echo $tag->term_id ?>"
                    class="all-projects__tag"
                >
                  <?php echo $tag->name ?>
                </a>
              <?php } ?>
            <?php } ?>
          </div>

        </div>

      <?php } ?>

    </div>

    <?php if ( get_sub_field( 'view_all_link' ) ) { ?>
      <a
          href="<?php echo get_sub_field( 'view_all_link' )['url'] ?>"
          class="more-projects__all title title--center title--block title--sm"
          target="<?php echo get_sub_field( 'view_all_link' )['target'] ?>"
      >
        <?php echo get_sub_field( 'view_all_link' )['title'] ?>
      </a>
    <?php } ?>

  </div>

  <?php wp_reset_query(); ?>

</section>
