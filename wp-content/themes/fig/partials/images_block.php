<section class="<?php echo count( get_sub_field( 'images' ) ) !== 1 ? 'container container--lrg images-block' : null ?><?php echo count( get_sub_field( 'images' ) ) === 2 ? ' images-block--double' : ' images-block--triple'; ?>">

  <?php if ( count( get_sub_field( 'images' ) ) === 4 ) { ?>

    <?php foreach ( get_sub_field( 'images' ) as $key => $image ) { ?>

      <?php if ( $key === 0 || $key === 1 ) { ?>
        <div
            class="lazy-container lazyload images-block__flex-container"
            data-expand="-100"
        >
          <span class="lazy-bg lazy-bg--outh"></span>
          <img
              data-src="<?php echo $image['sizes']['image-block-md'] ?>"
              alt="<?php echo $image['title']; ?>"
              class="images-block__image lazy-img lazy-img--outh lazyload"
              data-expand="200"
          >
        </div>
      <?php } ?>

      <?php if ( $key === 2 ) { ?>
        <div class="images-block__col lazyload lazy-container" data-expand="-100">
      <?php } ?>

      <?php if ( $key === 2 || $key === 3 ) { ?>
        <div class="images-block__flex-container">
          <span class="lazy-bg lazy-bg--outh"></span>
          <img
              data-src="<?php echo $image['sizes']['image-block-sm'] ?>"
              alt="<?php echo $image['title']; ?>"
              class="images-block__image images-block__image--half-height lazy-img lazy-img--outh lazyload"
              data-expand="200"
          >
        </div>
      <?php } ?>

      <?php if ( $key === 3 ) { ?>
        </div>
      <?php } ?>

    <?php } ?>

  <?php } ?>

  <?php if ( count( get_sub_field( 'images' ) ) === 3 ) { ?>

    <?php foreach ( get_sub_field( 'images' ) as $key => $image ) { ?>
      <div
          class="lazy-container lazyload"
          data-expand="-100"
      >
        <span class="lazy-bg lazy-bg--outh"></span>
        <img
            data-src="<?php echo $image['sizes']['image-block-md'] ?>"
            alt="<?php echo $image['title']; ?>"
            class="images-block__image lazy-img lazy-img--outh lazyload"
            data-expand="200"
        >
      </div>
    <?php } ?>

  <?php } ?>

  <?php if ( count( get_sub_field( 'images' ) ) === 2 ) { ?>

    <?php foreach ( get_sub_field( 'images' ) as $key => $image ) { ?>
      <div
          class="lazy-container lazyload"
          data-expand="-100"
      >
        <span class="lazy-bg lazy-bg--outh"></span>
        <picture>
          <source
              data-srcset="<?php echo $key === 0 ? $image['sizes']['image-block-lrg-mobile'] : $image['sizes']['image-block-md'] ?>"
              media="(max-width: 640px)"
          >
          <img
              data-src="<?php echo $key === 0 ? $image['sizes']['image-block-lrg'] : $image['sizes']['image-block-md'] ?>"
              class="images-block__image lazy-img lazy-img--outh lazyload"
              data-expand="200"
              alt="<?php echo $image['title']; ?>"
          />
        </picture>
      </div>
    <?php } ?>

  <?php } ?>

  <?php if ( count( get_sub_field( 'images' ) ) === 1 ) { ?>
    <div
        class="lazy-container lazyload"
        data-expand="-100"
    >
      <span class="lazy-bg lazy-bg--outh"></span>
      <picture>
        <source
            data-srcset="<?php echo get_sub_field( 'images' )[0]['sizes']['image-slider-mobile'] ?>"
            media="(max-width: 640px)"
        >
        <img
            data-src="<?php echo get_sub_field( 'images' )[0]['sizes']['image-slider'] ?>"
            class="images-block__image images-block__image--full lazy-img lazy-img--outh lazyload"
            data-expand="200"
            alt="<?php echo get_sub_field( 'images' )[0]['alt'] ?>"
        />
      </picture>
    </div>
  <?php } ?>

</section>
