<section>

  <div class="container container--md">

    <?php if ( get_sub_field( 'title' ) ) { ?>
      <h2 class="title title--center">
        <?php echo get_sub_field( 'title' ); ?>
      </h2>
    <?php } ?>

    <div class="clients-list">

      <?php foreach ( get_sub_field( 'client_logos' ) as $image ) { ?>

        <div
            class="lazy-container lazyload clients-list__logo"
            data-expand="-100"
        >
          <picture>
            <source
                data-srcset="<?php echo $image['sizes']['clients-image-mobile'] ?>"
                media="(max-width: 640px)"
            >
            <img
                data-src="<?php echo $image['sizes']['thumbnail'] ?>"
                class="lazy-img lazy-img--fade lazyload"
                data-expand="200"
                alt="<?php echo $image['alt'] ?>"
            />
          </picture>
        </div>

      <?php } ?>

    </div>

  </div>

</section>
