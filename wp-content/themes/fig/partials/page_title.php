<section class="page-title<?php echo get_sub_field( 'position' ) === 'center' ? ' page-title--center' : null; ?><?php echo get_sub_field( 'search_projects_button' ) ? ' page-title--sm' : null; ?><?php echo get_sub_field('spacing_size') === 'small' ? ' page-title--spacing-sm' : null ?>">

  <div class="container container--md">
    <div class="page-title__inner">
      <?php if ( get_sub_field( 'sub_title' ) ) { ?>
        <h2 class="page-title__sub-title">
          <?php echo get_sub_field( 'sub_title' ) ?>
        </h2>
      <?php } ?>
      <?php if ( get_sub_field( 'title' ) ) { ?>
        <h1 class="title">
          <?php echo get_sub_field( 'title' ) ?>
        </h1>
      <?php } ?>

      <?php if ( get_sub_field( 'search_projects_button' ) ) { ?>
        <button class="btn js-open-search-window">
          Search Projects
        </button>
      <?php } ?>
    </div>
  </div>

</section>

<?php if ( get_sub_field( 'search_projects_button' ) ) {
  include_once "search-window.php";
} ?>
