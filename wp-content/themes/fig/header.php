<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta
      name="viewport"
      content="width=device-width, initial-scale=1"
  >
  <link
      rel="profile"
      href="https://gmpg.org/xfn/11"
  >

  <title>
    <?php echo wp_title(); ?>
  </title>

  <?php
  wp_enqueue_style( 'main', get_template_directory_uri() . '/dist/main.css' );
  wp_head();
  ?>
</head>
<body>

<header
    class="header-container"
    id="js-header-wrapper"
>

  <div class="header">

    <div
        class="header-container__inner"
        id="js-header-container"
    >
      <div class="header__links lazy-content-container lazy-content-container--vertical lazyload">

        <a
            href="/"
            class="header__logo"
        >
          <?php
          $logo1 = true;
          $logo2 = true;

          if ( ! get_fields()['layout'] ) {
            echo '<img src="' . get_template_directory_uri() . "/src/img/logo.svg" . '">';
          } else {
            if ( get_fields()['layout'][0]['acf_fc_layout'] === 'hero' || get_fields()['layout'][0]['acf_fc_layout'] === 'images_block' ) {
              if ( $logo1 === true ) {
                echo '<img src="' . get_template_directory_uri() . "/src/img/logo-alt.svg" . '">';
                $logo1 = false;
                $logo2 = false;
              }
              echo '<script>
                document.getElementById("js-header-wrapper").classList.add("header-container--alt");
              </script>';
            } else {
              if ( $logo2 === true ) {
                echo '<img src="' . get_template_directory_uri() . "/src/img/logo.svg" . '">';
                $logo2 = false;
                $logo1 = false;
              }
            }
          }

          ?>
        </a>

        <?php if ( wp_get_nav_menu_items( 'header-menu' ) ) { ?>
          <?php foreach ( wp_get_nav_menu_items( 'header-menu' ) as $item ) { ?>
            <a
                href="<?php echo $item->url; ?>"
                target="<?php echo $item->target ?>"
                class="header__links__link"
            >
              <?php echo $item->title; ?>
            </a>
          <?php } ?>
        <?php } ?>

      </div>

    </div>

    <button
        class="menu-btn lazy-content-container lazy-content-container--vertical lazyload"
        id="js-menu-btn"
    >
      <span class="menu-btn__text">
        Menu
      </span>
      <span class="menu-toggle"></span>
    </button>

    <?php if ( wp_get_nav_menu_items( 'header-menu' ) ) { ?>

      <nav
          class="mobile-menu"
          id="js-mobile-menu"
      >

        <ul class="mobile-menu__links">
          <?php foreach ( wp_get_nav_menu_items( 'mobile-menu' ) as $item ) { ?>
            <li class="mobile-menu__link">
              <a
                  href="<?php echo $item->url; ?>"
                  target="<?php echo $item->target ?>"
              >
                <?php echo $item->title; ?>
              </a>
            </li>
          <?php } ?>
        </ul>

      </nav>

    <?php } ?>

  </div>

</header>
