<footer class="footer">

  <div class="container container--lrg">

    <div class="footer__inner">

      <div class="footer__upper">

        <h2 class="title">
          <?php echo get_field( 'footer_title', 'option' ) ?>
        </h2>

      </div>

      <div class="footer__mid">

        <div class="footer-locations">

          <?php if ( get_field( 'location', 'option' ) ) { ?>
            <?php foreach ( get_field( 'location', 'option' ) as $location ) { ?>

              <div class="footer-location<?php echo ' footer-location--' . $location['height'] ?>">
                <h3 class="footer-location__title">
                  <?php echo $location['title']; ?>
                </h3>

                <address class="footer-location__address">
                  <?php echo $location['address']; ?>
                </address>

                <?php if ( $location['phone_number'] ) { ?>
                  <a
                      href="tel:<?php echo $location['phone_number']; ?>"
                      class="footer-location__link"
                  >
                    <?php echo $location['phone_number']; ?>
                  </a>
                <?php } ?>

                <?php if ( $location['email_address'] ) { ?>
                  <a
                      href="mailto:<?php echo $location['email_address']; ?>"
                      class="footer-location__link"
                  >
                    <?php echo $location['email_address']; ?>
                  </a>
                <?php } ?>
              </div>

            <?php } ?>
          <?php } ?>

        </div>

      </div>

      <div class="footer__lower">
        <a
            href="/"
            class="footer__logo"
        >
          <?php echo file_get_contents( get_template_directory() . "/src/img/logo-alt.svg" ); ?>
        </a>

        <div class="footer__social">

          <a href="/privacy-policy" style="margin-right: 20px;">
            Privacy Policy
          </a>

          <?php if ( get_field( 'instagram', 'option' ) ) { ?>
            <a
                href="https://instagram.com/<?php echo get_field( 'instagram', 'option' ); ?>"
                class="footer__social-icon footer__link"
                target="_blank"
                rel="nofollow noreferrer"
            >
              <?php echo file_get_contents( get_template_directory() . "/src/img/instagram.svg" ); ?>
            </a>
          <?php } ?>

          <?php if ( get_field( 'twitter', 'option' ) ) { ?>
            <a
                href="https://twitter.com/<?php echo get_field( 'twitter', 'option' ); ?>"
                class="footer__social-icon footer__link"
                target="_blank"
                rel="nofollow noreferrer"
            >
              <?php echo file_get_contents( get_template_directory() . "/src/img/twitter.svg" ); ?>
            </a>
          <?php } ?>

          <?php if ( get_field( 'facebook', 'option' ) ) { ?>
            <a
                href="https://facebook.com/<?php echo get_field( 'facebook', 'option' ); ?>"
                class="footer__social-icon footer__link"
                target="_blank"
                rel="nofollow noreferrer"
            >
              <?php echo file_get_contents( get_template_directory() . "/src/img/facebook.svg" ); ?>
            </a>
          <?php } ?>

          <?php if ( get_field( 'linkedin', 'option' ) ) { ?>
            <a
                href="https://linkedin.com/<?php echo get_field( 'linkedin', 'option' ); ?>"
                class="footer__social-icon footer__link"
                target="_blank"
                rel="nofollow noreferrer"
            >
              <?php echo file_get_contents( get_template_directory() . "/src/img/linkedin.svg" ); ?>
            </a>
          <?php } ?>

          <?php if ( get_field( 'youtube', 'option' ) ) { ?>
            <a
                href="<?php echo get_field( 'youtube', 'option' ); ?>"
                class="footer__social-icon footer__link"
                target="_blank"
                rel="nofollow noreferrer"
            >
              <?php echo file_get_contents( get_template_directory() . "/src/img/youtube.svg" ); ?>
            </a>
          <?php } ?>

        </div>
      </div>

    </div>

  </div>

</footer>

<div class="cookie-popup is-closed">
  <button class="js-close-cookies cookie-popup__close">
    &times;
  </button>
  <div>
    We use cookies to personalise content, provide social media features and to analyse our traffic. We also share information about your use of our site with our social media and analytics partners who may combine it with other information that you’ve provided to them or that they’ve collected from your use of their services. By using this website, you agree to the use of cookies as stipulated in our <a href="/privacy-policy">privacy policy.</a>
  </div>
  <button class="js-accept-cookies cookie-popup__accept">
    Accept Cookies
  </button>
</div>

<?php
wp_enqueue_script( 'main-js', get_template_directory_uri() . '/dist/main.js', null, null, true );
wp_footer();
?>

</body>
</html>
