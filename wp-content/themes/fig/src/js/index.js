import style from '../scss/style.scss';
import Video from "./Video";
import 'lazysizes';
import Header from "./Header";
import Sliders from "./Sliders";
import Lazy from "./Lazy";
import ScrollTo from './ScrollTo';
import Filters from './Filters';
// import Insta from './Insta';

new Video();
new Header();
new Sliders();
new Lazy();
new ScrollTo(document.body, 2000, 0, 'js-scroll-top');
new Filters();
// new Insta();

const vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);

const closeCookies = document.querySelector('.js-close-cookies');
const acceptCookies = document.querySelector('.js-accept-cookies');
const cookies = document.querySelector('.cookie-popup');
if (closeCookies && acceptCookies && cookies) {
  closeCookies.addEventListener('click', function () {
    cookies.classList.add('is-closed');
  });
  if (document.cookie.replace(/(?:(?:^|.*;\s*)cookiePopup\s*\=\s*([^;]*).*$)|^.*$/, "$1") !== "true") {
    cookies.classList.remove('is-closed');

    acceptCookies.addEventListener('click', function () {
      document.cookie = "cookiePopup=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
      cookies.classList.add('is-closed');
    })

  }
}
