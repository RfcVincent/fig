import Flickity from "flickity-fade";

const lazyBG = require('flickity-bg-lazyload');

export default class Slider {

  constructor(slider) {
    this.sliderContainer = slider;

    this.slider = new Flickity(this.sliderContainer.querySelector('.js-slider'), {
      contain: true,
      fade: true,
      bgLazyLoad: true,
      prevNextButtons: false,
      wrapAround: true,
      pageDots: this.sliderContainer.querySelectorAll('.image-slider__slide').length > 1
    });

    this.next = this.sliderContainer.querySelector('.js-slider-next');
    this.prev = this.sliderContainer.querySelector('.js-slider-prev');

    if (this.next && this.prev) {
      this.sliderControlEvents();
    }

  }

  sliderControlEvents() {
    this.prev.addEventListener('click', () => this.slider.previous());
    this.next.addEventListener('click', () => this.slider.next());
  }

}
