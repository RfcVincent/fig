export default class Insta {

  constructor() {
    const feed = document.querySelector('.js-instafeed');
    const more = document.querySelector('.js-insta-more');

    if(!feed || !more) return;

    more.addEventListener('click', () => this.loadMore());
  }

  loadMore() {
    fetch(`https://api.instagram.com/v1/users/2302284817/media/recent/?access_token=2302284817.224a9af.16b63edb0a4240d2a24785033ff5407c&count=18`)
    .then(
      function(response) {
        if (response.status !== 200) {
          console.log('Looks like there was a problem. Status Code: ' +
            response.status);
          return;
        }

        // Examine the text in the response
        response.json().then(function(data) {
          console.log(data);
          // console.log(data.pagination.next_max_id);
          // feed.dataset.last = data.pagination.next_max_id;

          data.data.forEach(item => {
            feed.innerHTML += `
     <a
            class="lazy-container lazyload social-block__image"
            data-expand="-100"
            href="${item.link}"
        >
          <img
              data-src="${item.images.standard_resolution.url}"
              class="lazyload"
          >

        </a>
    `
          })

        });
      }
    )
    .catch(function(err) {
      console.log('Fetch Error :-S', err);
    });
  }

}
