/**
 * @fileOverview Class representing an optimised/debounced window resize/scroll event, with support for an array of
 * callback functions
 * @author Ritchie Vincent
 * @version 1.2
 */
/**
 * @example
 * import OptimisedResize from './classes/OptimisedResize';
 *
 * const optimisedResize = new OptimisedResize();
 * OR
 * const optimisedResize = new OptimisedResize({
 *     scroll: true,
 * });
 *
 * optimisedResize.addFunction(() => {
 *     run callback code here
 * });
 */
class OptimisedResize {

  constructor(options = {}) {

    /**
     * @type {Array}
     * @default
     */
    this.callbacks = [];

    /**
     * @type {boolean}
     * @default
     */
    this.running = false;

    /**
     * @type {undefined}
     */
    this.resizeTimer = undefined;

    options.customTime = options.customTime || options.customTime === 0 ? options.customTime : 250;

    this.options = options;

  }

  /**
   * This function is fired on every window resize or scroll event. The event is throttled, to avoid performance
   * issues with the event being fired too often.
   * @since 1.2
   */
  event() {

    //Only run if it's not already running
    if (!this.running) {

      this.running = true;

      //Run the callbacks using requestAnimationFrame for performance reasons
      window.requestAnimationFrame(this.runCallbacks.bind(this));

    }

  }

  /**
   * Run any callbacks supplied, debouncing the method to ensure it only runs when the user stops resizing the window
   */
  runCallbacks() {

    this.callbacks.forEach(callback => {

      clearTimeout(this.resizeTimer);
      this.resizeTimer = setTimeout(() => {

        callback();

      }, this.options.customTime);

    });

    this.running = false;

  }

  /**
   * Adds a callback to the callback array
   * @param {function} callback - A callback to be run
   */
  addCallback(callback) {

    if (callback) this.callbacks.push(callback);

  }

  /**
   * Checks if there are any callback functions, and runs them on every resize/scroll event on the window. If
   * there are no callbacks currently in the array, it will add the supplied one
   * @param {function} callback - A callback to add to the array
   */
  addFunction(callback) {

    if (!this.callbacks.length) {

      if (this.options.scroll) {

        window.addEventListener('scroll', this.event.bind(this));

      } else {

        window.addEventListener('resize', this.event.bind(this));

      }

    }

    this.addCallback(callback);

  }

}

export default OptimisedResize;
