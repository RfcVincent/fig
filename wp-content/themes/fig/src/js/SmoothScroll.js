export default class SmoothScroll {

    constructor(options) {

        this.defaults = {
            element: document.body, //The element to scroll to
            duration: 3000, //The duration of the animation in ms
            offset: 0 //The offset of the scroll in pixels
        };

        this.options = {...this.defaults, ...options};

    }

    scroll() {

        const pageY = window.pageYOffset;
        const bodyHeight = document.body.scrollHeight;
        const innerHeight = window.innerHeight;
        const startingY = pageY + this.options.offset;
        const elementY = pageY + this.options.element.getBoundingClientRect().top;
        const targetY = bodyHeight - elementY < innerHeight ? bodyHeight - innerHeight : elementY;
        const diff = targetY - startingY;
        const easing = t => t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
        let start;

        if (!diff) return;

        const self = this;

        window.requestAnimationFrame(function step(timestamp) {

            if (!start) start = timestamp;

            const time = timestamp - start;
            let percent = Math.min(time / self.options.duration, 1);

            percent = easing(percent);

            const end = (startingY + diff * percent) - self.options.offset;

            window.scrollTo(0, end);

            if (time < self.options.duration) window.requestAnimationFrame(step);

        });

    }

}
