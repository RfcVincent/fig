export default class Filters {

  constructor() {
    this.window = document.querySelector('.js-search-window');
    this.openWindowBtns = [...document.querySelectorAll('.js-open-search-window')];
    this.closeWindowBtns = [...document.querySelectorAll('.js-close-search-window')];
    this.items = [...document.querySelectorAll('.js-search-item')];
    this.clearFilters = document.querySelectorAll('.js-clear-all-filters');

    if (this.window && this.openWindowBtns && this.closeWindowBtns) {
      this.init();
    }

  }

  openWindow() {
    this.window.classList.add('is-active');
    document.body.classList.add('no-scroll');
  }

  closeWindow() {
    this.window.classList.remove('is-active');
    document.body.classList.remove('no-scroll');
  }

  loopOpenToggles() {
    this.openWindowBtns.forEach(btn => {
      btn.addEventListener('click', () => this.openWindow());
    })
  }

  loopCloseToggles() {
    this.closeWindowBtns.forEach(btn => {
      btn.addEventListener('click', () => this.closeWindow());
    })
  }

  loopItems() {

    this.items.forEach(item => {

      if (window.location.search.indexOf(item.dataset.value) !== -1) {
        item.classList.add('is-active');

        item.addEventListener('click', e => {
          let regex = window.location.search.replace(new RegExp(`&${e.target.dataset.value}`, "g"), "");
          regex = regex.replace(new RegExp(`/?${e.target.dataset.value}`, "g"), "");
          window.location.search = regex;
        })

      }

      item.addEventListener('click', e => {

        if (window.location.pathname !== '/work/') {
          window.history.pushState({}, null, '/work/');
          window.location.search += `&${e.target.dataset.value}`;
          return;
        }

        if (window.location.search.indexOf(e.target.dataset.value) === -1) {
          window.location.search += `&${e.target.dataset.value}`;
        }

      })
    })

  }

  init() {
    this.loopOpenToggles();
    this.loopCloseToggles();
    this.loopItems();

    if (this.clearFilters) {
      this.clearFilters.forEach(clear => {
        clear.addEventListener('click', () => {
          window.location.search = '';
        })
      })
    }

  }

}
