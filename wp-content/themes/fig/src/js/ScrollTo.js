import SmoothScroll from './SmoothScroll';

export default class ScrollTo {

  constructor(element = document.body, duration = 3000, offset = 0, button = null) {
    this.element = element;
    this.duration = duration;
    this.offset = offset;
    this.buttons = [...document.querySelectorAll(`.${button}`)];

    this.buttons && this.init();
  }

  clickEventListener() {
    new SmoothScroll({element: this.element, duration: this.duration, offset: this.offset}).scroll();
  }

  loopBtns(cb) {
    this.buttons.forEach(btn => cb(btn));
  }

  init() {
    this.loopBtns(btn => btn.addEventListener('click', () => this.clickEventListener()));
  }

}
