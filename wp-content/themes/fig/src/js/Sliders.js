import Slider from "./Slider";

export default class Sliders {

  constructor() {
    document.querySelectorAll('.js-slider-container').forEach(slider => {
      new Slider(slider);
    });
  }

}
