export default class Video {

  constructor() {
    this.videos = [...document.querySelectorAll('.js-video')];

    if (this.videos.length) this.init();

  }

  addEventListener(video) {
    video.addEventListener('click', () => video.classList.add('is-active'));
  }

  loopVideos(cb) {
    this.videos.forEach(video => cb(video));
  }

  init() {
    this.loopVideos(video => this.addEventListener(video));
  }

}
