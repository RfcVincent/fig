import OptimisedResize from "./OptimisedResize";

export default class Header {

  constructor() {
    this.optimisedResize = new OptimisedResize({
      scroll: true,
      customTime: 0,
    });

    this.headerContainer = document.getElementById('js-header-container');
    this.menuBtn = document.getElementById('js-menu-btn');
    this.mobileMenu = document.getElementById('js-mobile-menu');

    if (this.optimisedResize && this.headerContainer && this.menuBtn) this.init();

  }

  closeMenu() {
    document.body.classList.remove('no-scroll');
    this.menuBtn.classList.remove('is-active');
    this.mobileMenu.classList.remove('is-active');
    if(window.scrollY < 10) {
      this.showDeskMenu();
    }
  }

  openMenu() {
    document.body.classList.add('no-scroll');
    this.menuBtn.classList.add('is-active');
    this.mobileMenu.classList.add('is-active');
    this.hideDeskMenu();
  }

  hideDeskMenu() {
    this.menuBtn.classList.add('is-fixed');
    this.headerContainer.classList.add('is-hidden');
  }

  showDeskMenu() {
    this.menuBtn.classList.remove('is-fixed');
    this.headerContainer.classList.remove('is-hidden');
  }

  scrolledMenu() {
    if (window.scrollY > 10) {
      this.hideDeskMenu();
      return;
    }
    this.showDeskMenu();
  }

  init() {
    this.optimisedResize.addFunction(() => this.scrolledMenu());
    this.scrolledMenu();

    this.menuBtn.addEventListener('click', () => {
      this.menuBtn.classList.contains('is-active') ? this.closeMenu() : this.openMenu();
    })

  }

}
