<?php get_header(); ?>

<div class="page-contents">

  <div class="container container--md news-page">

    <h1 class="title">
      <?php the_title(); ?>
    </h1>

    <?php the_content(); ?>

    <button
        class="btn"
        onclick="window.history.back();"
        style="margin-top: 40px;"
    >
      Back to news
    </button>

  </div>

</div>

<?php get_footer(); ?>
