<?php

add_theme_support( 'post-thumbnails' );

if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'image-block-lrg', 884, 610, true );
  add_image_size( 'image-block-lrg-mobile', 640, 610, false );
  add_image_size( 'image-block-md', 442, 610, true );
  add_image_size( 'image-block-sm', 442, 290, true );
  add_image_size( 'image-slider', 1920, 900, true );
  add_image_size( 'image-slider-mobile', 640, 900, false );
  add_image_size( 'showcase-image', 840, 560, true );
  add_image_size( 'featured-image', 930, 360, true );
  add_image_size( 'featured-image-mobile', 640, 360, true );
  add_image_size( 'location-image', 560, 610, true );
  add_image_size( 'news-image-lrg', 678, 936, true );
  add_image_size( 'news-image-med', 678, 453, true );
  add_image_size( 'news-image-sm', 442, 453, true );
  add_image_size( 'work-image', 630, 420, true );
  add_image_size( 'clients-image-mobile', 100, 100, true );
}

