<?php

if (function_exists('acf_add_options_page')) {
	acf_add_options_page('Site Options');
	acf_add_options_page([
	  'page_title' => 'Locations',
    'icon_url' => 'dashicons-admin-site',
  ]);
  acf_add_options_page([
    'page_title' => 'Footer',
    'icon_url' => 'dashicons-editor-insertmore',
  ]);
}

// Remove categories / tags from Posts
function unregister_tags() {
	unregister_taxonomy_for_object_type( 'post_tag', 'post' );
	unregister_taxonomy_for_object_type( 'category', 'post' );
}
add_action( 'init', 'unregister_tags' );

// Disable Comments
add_action( 'admin_menu', 'remove_comment_menu_item' );
function remove_comment_menu_item() {
    remove_menu_page( 'edit-comments.php' );
}
add_action('init', 'remove_comment_support', 100);
function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}
add_action( 'wp_before_admin_bar_render', 'remove_comment_admin_bar' );
function remove_comment_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}

add_action( 'admin_init', 'remove_textarea' );

//Remove text area from all pages
function remove_textarea() {
    remove_post_type_support( 'page', 'editor' );
}


//add_filter( 'wpseo_metabox_prio', 'low' );
