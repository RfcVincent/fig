<?php

function register_posttypes() {
  register_post_type( 'projects',
    array(
      'labels'       => array(
        'name'          => __( 'Projects' ),
        'singular_name' => __( 'Project' )
      ),
      'public'       => true,
      'hierarchical' => false,
      'has_archive'  => true,
      'menu_icon'    => 'dashicons-feedback',
      'supports'     => array( 'thumbnail', 'title' ),
      'taxonomies'   => array( 'category' )
    )
  );
  register_post_type( 'news',
    array(
      'labels'       => array(
        'name'          => __( 'News' ),
        'singular_name' => __( 'News' )
      ),
      'public'       => true,
      'hierarchical' => false,
      'has_archive'  => false,
      'menu_icon'    => 'dashicons-media-document',
      'supports'     => array( 'title', 'editor', 'thumbnail' ),
    )
  );
}

add_action( 'init', 'register_posttypes' );

