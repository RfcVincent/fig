<?php

// Setup navigation
function register_menus() {
  register_nav_menu( 'header-menu', __( 'Header Menu' ) );
  register_nav_menu( 'mobile-menu', __( 'Mobile Menu' ) );
  register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );
}

add_action( 'after_setup_theme', 'register_menus' );

// Custom excerpt length
function custom_excerpt_length( $length ) {
  //return 10;
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Change end of excerpt
function new_excerpt_more( $more ) {
  //return '...';
}

add_filter( 'excerpt_more', 'new_excerpt_more' );

//Remove wp-emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

//Remove wp-embed
function my_deregister_scripts() {
  wp_deregister_script( 'wp-embed' );
}

add_action( 'wp_footer', 'my_deregister_scripts' );


add_filter( 'acf/load_value/name=layout', 'add_starting_repeater', 10, 3 );
function add_starting_repeater( $value, $post_id, $field ) {
  if ( $value !== null ) {
    return $value;
  }
  $value = [
    [
      'acf_fc_layout' => 'hero'
    ],
    [
      'acf_fc_layout' => 'content_block'
    ],
    [
      'acf_fc_layout' => 'images_block'
    ],
    [
      'acf_fc_layout' => 'content_block'
    ],
    [
      'acf_fc_layout' => 'video_block'
    ],
    [
      'acf_fc_layout' => 'content_block'
    ],
    [
      'acf_fc_layout' => 'content_block'
    ],
    [
      'acf_fc_layout' => 'images_block'
    ],
    [
      'acf_fc_layout' => 'content_block'
    ],
    [
      'acf_fc_layout' => 'images_block'
    ],
    [
      'acf_fc_layout' => 'quote_block'
    ],
    [
      'acf_fc_layout' => 'showcase_image'
    ],
    [
      'acf_fc_layout' => 'more_projects'
    ],
    [
      'acf_fc_layout' => 'contact_block'
    ],
  ];

  return $value;
}

function custom_theme_assets() {
  // bail if classic editor setting is set to block or no-replace
  $option = get_option( 'classic-editor-replace' );
  if ( $option === 'block' || $option === 'no-replace' ) {
    return;
  }

  // bail if classic editor plugin is not active. can't use is_plugin_active() here, unless you include wp-admin/includes/plugin.php
  if ( ! in_array( 'classic-editor/classic-editor.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    return;
  }

  // dequeue the block library, so that you don't have /wp-includes/css/dist/block-library/style.min.css in each and every page of your site
  wp_dequeue_style( 'wp-block-library' );
}
add_action( 'wp_enqueue_scripts', 'custom_theme_assets', 100 );


function post_remove ()      //creating functions post_remove for removing menu item
{
  remove_menu_page('edit.php');
}

add_action('admin_menu', 'post_remove');   //adding action for triggering function call


function vnmFunctionality_embedWrapper($html, $url, $attr, $post_id) {
  return '<div class="iframe-wrapper">' . $html . '</div>';
}

add_filter('embed_oembed_html', 'vnmFunctionality_embedWrapper', 10, 4);

function wp_refresh_insta_token() {
  $cURLConnection = curl_init();
	$access_token = get_transient( 'insta_access_token' );
    curl_setopt($cURLConnection, CURLOPT_URL, "https://graph.instagram.com/refresh_access_token?grant_type=ig_refresh_token&access_token=" . $access_token);
    curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($cURLConnection);
    curl_close($cURLConnection);

    if ( $result ) {
      $jsonArrayResponse = json_decode($result);

      $access_token = $jsonArrayResponse->access_token;
      set_transient( 'insta_access_token', $access_token, '25920000000' );
    }
}
add_action('wp_refresh_insta_token_action', 'wp_refresh_insta_token');

// Disable auto-update email notifications for plugins.
add_filter( 'auto_plugin_update_send_email', '__return_false' );
 
// Disable auto-update email notifications for themes.
add_filter( 'auto_theme_update_send_email', '__return_false' );