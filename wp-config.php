<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J/ds/9lHbqxP+AWRYSMgxmCRI2hjCcv0ibo7UHVM8mTU+MSX7gsRJq3MmzkSbmM8fYAcp5Wpnq8hX55xWjYRGg==');
define('SECURE_AUTH_KEY',  '0Pt9cj4t2cVNow70qNbpCvgrbVi2Mwb7tfghcffETjUh78Ntl2UknnPDW+GtwnFgKoXI0P2+svphglkRmUQ/OA==');
define('LOGGED_IN_KEY',    'fVVnLe3P8FTDnpSZmt6JHdwPQ9uklbwSEfecg5kSeInAd2is6vH+ENOQ20CAFUEII3aD0sQk4ETyUIBVbBFP+g==');
define('NONCE_KEY',        'p5e5lzQVmb65uOLsQNycf5Ahi5iugVTZHfkgHKAQRTphQqQ2gpsmcDqk93WX851FKCYtVZ6fer1Fber/4Xbmng==');
define('AUTH_SALT',        'aJvyv3cMzZsmKMuBcYHSLRWl+OWkEN6eNBbsIAka1tL5BWMyPlJepBTv2H4LF4ytSZAufRB0Dxy2xCdCWLSYzQ==');
define('SECURE_AUTH_SALT', 'YQPclCEu/oc9Mboc05tjf3c6wDn4EQqjSi4cvfOaB4HEPJzp29Fofe8Pj8RM2GCSxAd/uT3rAmz2L5oN2X7vjg==');
define('LOGGED_IN_SALT',   '1VA3jvjOCa7wPRaXY9hDgjSQOh21Uet40+Ipy3WkngYL7djUX+KapIrg0l1j9eaCgz1G7PJ/72T2/5Nj2GpBkQ==');
define('NONCE_SALT',       'omAQI4DTodRu7ixRliEHD4711X3KvR+vMBndqHMzEVEytOyfK6xCs9XGf2eppogdHbAu7adT0A0+AN9X3HKJIw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
